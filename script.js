jQuery(document).ready(function($) {
    // add editlink classes
    $('.alfred').each(function() {
        // add element to parent to trigger mousehover
        if($(this).parent().is('tr')) {
            $(this).parent().addClass('alfredparent');
        }
        else {
            $(this).parent().wrapInner('<div class="alfredparent"></div>');
        }
    });

    // handle modal close events
    $('a[data-reload]').on('pw-modal-closed', function() {
        $('body').prepend('<div id="alfredreload"></div>');
        $('#alfredreload').fadeIn();
        location.reload();
    });
});