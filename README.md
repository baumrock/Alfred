# Alfred - Another Light FRontend EDitor

## Setup

### Step 1: Install the Module "PageFrontEdit"

Also make sure that the user has appropriate permissions "page-edit-front"

### Step 2: Install Alfred

### Step 3: Initialize the module and load all your styles
I'm using AIOM so I can do this:

```
// load alfred and its assets
$config->alfred = $modules->get('Alfred');
$config->alfred->loadAssets();
```

That will execute this method:

```
public function loadAssets() {
    $this->config->styles->append('../modules/Alfred/style.css');
    $this->config->scripts->append('../modules/Alfred/script.js');
}
```

So you need to make sure that those two files get loaded in your frontend, for example:

```
<link rel="stylesheet" href="<?= AIOM::CSS($config->styles->unique()); ?>">
<script src="<?= AIOM::JS($config->scripts->unique()); ?>"></script>
```

**You also need to load JQuery if it is not already loaded on your website!**

### Step 4: Add Alfred to your Frontend
You can add Alfred to any element you want to make editable. It will only show up if the user has the appropriate rights for the page.

```
<?= $config->alfred->render($page) ?>
```

or like this
```
<ul>
    <?php foreach($items as $item): ?>
        <li>
            <?= $config->alfred->render($item) ?>
            <!-- some more markup -->
        </li>
    <?php endforeach; ?>
</ul>
```

## Options

You can set the icons like this:

```
$config->alfred->iconEdit = 'uk-icon-edit';
$config->alfred->iconFile = 'uk-icon-file-o';
```