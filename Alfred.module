<?php namespace ProcessWire;
/**
 * Bernhard Baumrock 2016, baumrock.com
 */
class Alfred extends WireData implements Module {

    public $iconEdit = 'uk-icon-edit';
    public $iconFile = 'uk-icon-file-o';

    /**
     * getModuleInfo is a module required by all modules to tell ProcessWire about them
     *
     * @return array
     *
     */
    public static function getModuleInfo() {

        return array(
            'title' => 'ALFRED - Another Lovely FRontend EDitor', 
            'version' => 4,
            'summary' => 'Adds special Links for Admin-Tasks on hover',
            'singular' => true, 
            'autoload' => true,
            'requires'  => 'ProcessWire>=2.8.1, PageFrontEdit',
            );
    }

    public function init() {
        $this->addHookAfter('Page::render', $this, 'loadModalFiles');
    }

    /**
     * loadModalFiles adds <edit title></edit> if it finds any alfred-links
     */
    public function loadModalFiles($event) {
        $page = $event->object; 
        if($page->template == 'admin') return;

        $html = $event->return;
        if(strpos($html, 'class="alfred" id="alfred_'))
            $event->return = str_replace("</body>", "<edit title></edit></body>", $event->return); 
    }

    /**
     * render Alfred markup
     */
    public function render($page, $options = array()) {
        if($page->editable()) {

            $config = $this->wire->config;
            $pages = $this->wire->pages;
            $user = $this->wire->user;

            $fields = '';
            if(isset($options['fields']) AND is_array($options['fields'])) {
                $fields = '&fields=' . implode(',', $options['fields']);
            }
            
            ob_start();
            $editid = "alfred_" . uniqid();
            ?>
                <div class="alfred" id="<?= $editid ?>">
                    <div class="alfredbuttons">
                        
                        <a href="<?= $pages->get(2)->url ?>page/edit/?id=<?= $page->id . $fields ?>" class="pw-modal" data-buttons="#submit_save" data-autoclose data-reload>
                            <i class="<?= $this->iconEdit ?>"></i> <?= __('edit') ?>
                        </a>
                        
                        <?php
                        $templatefilepath = str_replace($config->paths->root, "", debug_backtrace()[0]['file']);
                        if($user->isSuperUser()) echo '<span><i class="' . $this->iconFile . '"></i> ' . $templatefilepath . '</span>';
                        ?>

                    </div>
                </div>
            <?php
            return ob_get_clean();
        }        
        
        return false;
    }

    /**
     * loads scripts and styles
     *
     * @return string HTML
     */
    public function loadAssets() {
        // paths relative to /site/templates/
        $this->config->styles->append('../modules/Alfred/style.css');
        $this->config->scripts->append('../modules/Alfred/script.js');
    }
}
